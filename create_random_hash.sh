#!/bin/bash


# you need have openssl installed
# you need https://github.com/libbitcoin/libbitcoin-explorer/wiki/Download-BX
# for dx

# nodejs , web3js



#echo " "
#echo "create a 128 bits (32 bytes/64 chars) sample string: "
# echo "random" | openssl dgst -sha256
DIGEST=$(openssl rand -hex 16 | openssl dgst -sha256)
# echo "check the length, must be 64"
#printf "87c1b129fbadd7b6e9abc0a9ef7695436d767aece042bec198a97e949fcbe14c" | wc -c
CHARS_COUNT=$(echo -n "$DIGEST" |wc -c | xargs echo )
if test  $CHARS_COUNT -eq 64  ; then
  echo $DIGEST
else
  exit 1
fi

# echo "convert into a hex file to prepare sha256"
# printf $( echo -n $DIGEST | sed 's/[[:xdigit:]]\{2\}/\\x&/g')
DIGEST_HEX=$( echo -n $DIGEST | sed 's/[[:xdigit:]]\{2\}/\\x&/g')


# echo "verify hex file content"
# printf $DIGEST_HEX | hexdump -C
# geth attach https://gmainnet.infura.io
#geth attach wss://mainnet.infura.io/ws/v3/fe552cc214704d1697b982cccb186e00



#geth --exec "eth.blockNumber" attach wss://mainnet.infura.io/ws/v3/fe552cc214704d1697b982cccb186e00
