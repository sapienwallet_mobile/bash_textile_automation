# Create new textile account
# new_textile_account.sh
# --> xpriv

# Create private key
# Convert private key to Ethereum address
# newaddress.sh
# --> priv xpriv xpub address

# Create, sign and send new Ethereum transaction to some address
# sendtransaction.sh to_address amount wif
# --> tx_hash

# Live feedback
# feedback.sh tx_hash number[1..5] text [xpriv]
# --> commit_hash


# Get feedback
# getfeedbacks.sh xpub invite [txhash]
# --> JSON feedbacks
