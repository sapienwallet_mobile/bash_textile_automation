#!/bin/bash


ADDRESS="0x6d2c7516c2653f08f1f7bd07cb7fd422e1c9496c"
XPUB="xpub6DAnVn15xLB3UCUUBqEXeDEVd5TNwSWC9oajY9i1hQbRnr8mvmJMk4s5YSDvQCcmrVXhganf1QB7E3fcnezQgDyWSXH7L7nV4HAVYzqvMCM"
# 0
BIP32_EX_HD_PUB_KEY=$( bx hd-public $XPUB)
echo $BIP32_EX_HD_PUB_KEY
# 0/0
ACCOUNT_XPUB=$(bx hd-public $BIP32_EX_HD_PUB_KEY)
echo "Account xpub:" $ACCOUNT_XPUB

ACCOUNT_PUB_KEY_COMPRESSED=$(bx base58-decode  $ACCOUNT_XPUB |tail -c 75 | head -c 66)
echo "Account pub compressed:"  $ACCOUNT_PUB_KEY_COMPRESSED

# ToDo: Convert compressed public key to uncompressed

ACCOUNT_PUB_KEY_UNCOMPRESSED=$(echo "
var EC = require('elliptic').ec;
var ec = new EC('secp256k1');
var key = ec.keyFromPublic('$ACCOUNT_PUB_KEY_COMPRESSED', 'hex');
var pubPoint = key.getPublic();
var pub = pubPoint.encode('hex');
console.log(pub);
" | node)

echo "Account pub uncompressed:" $ACCOUNT_PUB_KEY_UNCOMPRESSED



ACCOUNT_ETH_ADDRESS=$(echo $ACCOUNT_PUB_KEY_UNCOMPRESSED |  sed 's/^04//' | keccak-256sum -x -l | tr -d ' -' | tail -c 41)
echo "Account 0 Address: 0x$ACCOUNT_ETH_ADDRESS"

if [ $ADDRESS = $ACCOUNT_ETH_ADDRESS ]
then
  echo "Addresses are same"
fi
