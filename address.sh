#!/bin/bash

#brew install sha3sum
#https://github.com/libbitcoin/libbitcoin-explorer/wiki/Download-BX

# Useful to check: https://iancoleman.io/bip39/#english
# https://golos.io/@miratechnology/sozdanie-kriptovalyutnogo-koshelka-i-vsekh-ego-klyuchei-iz-komandnoi-stroki
# https://kobl.one/blog/create-full-ethereum-keypair-and-address/

# npm -g i web3

echo "Mnemonic Language: English"

RND=$(bx seed -b 128)
echo "random 128 bit:" $RND


MNEMONIC=$(bx mnemonic-new $RND)
echo "BIP 39 mnemonic:" $MNEMONIC

SEED=$(bx mnemonic-to-seed $MNEMONIC)
echo "SEED:" $SEED

HD_ROOT_PRIV_KEY=$(bx hd-new $SEED)
echo "BIP 32 Root Private Key:" $HD_ROOT_PRIV_KEY

HD_ROOT_PUB_KEY=$(bx hd-to-public $HD_ROOT_PRIV_KEY)
echo "BIP 32 Root Public Key:" $HD_ROOT_PUB_KEY

echo "Coin: Ethereum (60)"

EX_HD_PRIV_KEY=$(bx hd-private -i 44 -d $HD_ROOT_PRIV_KEY |  bx hd-private -i 60 -d | bx hd-private  -d)
echo "Account Extended Private Key [m/44'/60'/0']:" $EX_HD_PRIV_KEY
EX_HD_PUB_KEY=$(bx hd-to-public $EX_HD_PRIV_KEY)
echo "Account Extended Public Key:" $EX_HD_PUB_KEY

BIP32_EX_HD_PRIV_KEY=$(bx hd-private -i 44 -d $HD_ROOT_PRIV_KEY |  bx hd-private -i 60 -d | bx hd-private  -d | bx hd-private)
echo "BIP 32 Extended Private Key [m/44'/60'/0'/0]:" $BIP32_EX_HD_PRIV_KEY
BIP32_EX_HD_PUB_KEY=$(bx hd-to-public $BIP32_EX_HD_PRIV_KEY)
echo "BIP 32 Extended Public Key:" $BIP32_EX_HD_PUB_KEY

# m/44'/60'/0'/0/0
ACCOUNT_EX_PRIV_KEY=$(bx hd-private $BIP32_EX_HD_PRIV_KEY)
echo "Account 0 XPRIV:" $ACCOUNT_EX_PRIV_KEY
ACCOUNT_EX_PUB_KEY=$(bx hd-to-public $ACCOUNT_EX_PRIV_KEY)
echo "Account 0 XPUB:" $ACCOUNT_EX_PUB_KEY


ACCOUNT_PRIV_KEY=$(bx hd-to-ec $ACCOUNT_EX_PRIV_KEY)
echo "Account 0 Private Key:" $ACCOUNT_PRIV_KEY
ACCOUNT_PUB_KEY_COMPRESSED=$(bx ec-to-public $ACCOUNT_PRIV_KEY)
echo "Account 0 Publc Key Compressed:" $ACCOUNT_PUB_KEY_COMPRESSED
ACCOUNT_PUB_KEY_UNCOMPRESSED=$(bx ec-to-public -u $ACCOUNT_PRIV_KEY)
echo "Account 0 Publc Key Uncompressed:" $ACCOUNT_PUB_KEY_UNCOMPRESSED

ACCOUNT_ETH_ADDRESS=$(bx ec-to-public -u $ACCOUNT_PRIV_KEY |  sed 's/^04//' | keccak-256sum -x -l | tr -d ' -' | tail -c 41)
echo "Account 0 Address: 0x$ACCOUNT_ETH_ADDRESS"



# HD_PRIV=$(bx hd-private -i 44 -d $HD_ROOT_PRIV_KEY | bx hd-private -i 60 -d | bx hd-private -d  | bx hd-private )
# echo "HD Private Key:" $HD_PRIV
TX_HASH=$(echo "let Web3 = require('web3');

let privateKey = '0x$ACCOUNT_PRIV_KEY';
let apiUrl = 'https://ropsten.infura.io/v3/fe552cc214704d1697b982cccb186e00';
let to = '0xF0109fC8DF283027b6285cc889F5aA624EaC1F55';
let value = '1000000000';

let web3 = new Web3(apiUrl);

(async () => {
    let tx = await web3.eth.accounts.signTransaction({
            to: to,
            value: value,
            gas: 2000000
        },
        privateKey// privateKey
    );
    let status = await web3.eth.sendSignedTransaction(tx.rawTransaction);
    console.log(tx.transactionHash)
 })
().then(() => { })
" | node)
echo "Transaction hash:" $TX_HASH
